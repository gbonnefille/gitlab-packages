# [1.3.0](https://gitlab.com/to-be-continuous/gitlab-package/compare/1.2.2...1.3.0) (2025-01-27)


### Features

* disable tracking service by default ([df6e23f](https://gitlab.com/to-be-continuous/gitlab-package/commit/df6e23f39ca7ef403a2b16353973af92ab2f747b))

## [1.2.2](https://gitlab.com/to-be-continuous/gitlab-package/compare/1.2.1...1.2.2) (2024-10-04)


### Bug Fixes

* **release:** support full semantic-versioning specifcation (with prerelease and build metadata) ([dc232a1](https://gitlab.com/to-be-continuous/gitlab-package/commit/dc232a1a857ea7fbb3efd969303b30c910d5366e))

## [1.2.1](https://gitlab.com/to-be-continuous/gitlab-package/compare/1.2.0...1.2.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([04aca2e](https://gitlab.com/to-be-continuous/gitlab-package/commit/04aca2e0a457bf2fe0121ee25e3df0caeb099a75))

# [1.2.0](https://gitlab.com/to-be-continuous/gitlab-package/compare/1.1.0...1.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([30cb585](https://gitlab.com/to-be-continuous/gitlab-package/commit/30cb585e4eca3d7ab1a11b0b350fe0eaab734fce))

# [1.1.0](https://gitlab.com/to-be-continuous/gitlab-package/compare/1.0.0...1.1.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([b847266](https://gitlab.com/to-be-continuous/gitlab-package/commit/b84726630d6dc9339c8f4845b701c8025f41abd9))
